defmodule LiveChart do
  use LiveChartWeb, :live_component

  def get_max(data) do
    data
    |>Enum.zip
    |> Enum.map(fn x -> x |> Tuple.to_list() |> Enum.sum end)
    |> Enum.max(&>=/2, fn -> 0 end)
    |> Kernel.*(1.1)
  end

  def sum_of_stacks(data, index) do
      Enum.reduce(data, 0, fn dataset, acc -> Enum.at(dataset, index) + acc end)
  end



  def stack_sum_height(data, index) do
      case get_max(data) do
          0 -> 0
    	  x -> sum_of_stacks(data,index) * 100 / x
      end
  end

  def stack_height(data, dataset, index) do
      case sum_of_stacks(data, index) do
          0 -> 0
          x -> (Enum.at(dataset, index)*100 / x)
       end
  end

  def get_spacing(labels, max_labels) do
      Float.floor(length(labels) / max_labels) + 1
      |> trunc
  end

  def should_render_label(_labels, _max_labels, 0),  do: true
  def should_render_label(labels, max_labels, index) do
      cond do
      	length(labels) == index - 1 -> true
      	rem(index, get_spacing(labels, max_labels)) == 0 -> true
      	true -> false
      end
   end



  def render(assigns) do
    ~L"""
    	<table  style="display: block;  width:100%; border: none; padding:16px; padding-bottom: 25px;" >
    	<thead style="" >
    		<tr>
    			<th scope="col" style="font-weight: 300; border: none; padding: 12px;">
    				<div style="display: flex; align-items: center; justify-content: center; padding-top: 1em" >
    					<%= for {name,  color} <- @chart_input[:stacks] do %>
    						<div style="height:1em; width: 1em; background-color: <%= color %>;"> </div>
    						<div style="margin-right: 2em; margin-left: 0.5em"> <%= name %> </div>
    					<% end %>
    				</div>

    			</th>

    		</tr>
    	</thead>


            <style>#id29:before{ content: "<%= Float.floor(get_max(@chart_input[:data]))%>";
            		top: -0.6em;
    			position:absolute;
    			left:-3.2em;
    			width: 2.8em;
    			text-align:right;
    			font:bold 80%/120% arial,helvetica,sans-serif;

            }</style>

            <style>#id29:after{ content: "0";
            		bottom: -0.6em;
    			position:absolute;
    			left:-3.2em;
    			width: 2.8em;
    			text-align:right;
    			font:bold 80%/120% arial,helvetica,sans-serif;

            }</style>



    	<tbody id="id29" style="
    			position:relative;
    			display:grid;
    			grid-template-columns:repeat(auto-fit, minmax(1px, 1fr));
    			column-gap:1px;
    			align-items:end;
    			height:400px;
    			width: calc(100% -3em);
    			max-height:50vw;
    			margin-left: 3em;
    			padding: 0 1em;
    			border-bottom:2px solid rgba(0,0,0,0.5);
    			background:repeating-linear-gradient(
    				180deg,
    				rgba(170,170,170,0.7) 0,
    				rgba(170,170,170,0.7) 1px,
    				transparent 1px,
    				transparent 20%
    			);
    		}"

    		>

    		<%= for {label, index} <- Enum.with_index(@chart_input[:labels])  do %>

            		<tr style="height: <%= stack_sum_height(@chart_input[:data], index)%>%" ; position: relative; display: block
    		">

    			<%= for {dataset, stack_index} <-  Enum.with_index(@chart_input[:data]) do %>
    			<td style="
    				padding: 0px;
    				height: <%= stack_height(@chart_input[:data], dataset, index) %>% ;
    				width=100%; background-color: <%= @chart_input[:stacks] |> Enum.at(stack_index) |> elem(1) %>;
    				display: block;
    				text-align: center;
    				padding-top: 0px;

    				">
    				<%= if should_render_label(@chart_input[:labels],@max_labels,index) do %>
    				<span style= " font-size: 11px; border-radius: 3px; color: white;
    				text-shadow: -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000;
    				padding: 1px;
    				"
    				>
    					<%= case Enum.at(dataset,index) do %>
    					<% 0 -> %>
    					<% x -> %> <%= x %>
    					<% end %>

    				</span>

	    			<% end %>
    			</td>
    			<% end %>

    				<%= if should_render_label(@chart_input[:labels],@max_labels,index) do %>
	    				<th  style=" min-width: 3em; height:  padding: 0px; font-size: 11px; display:block; border: none; text-align: center; font-weight: 300; padding-top:1em;" scope="row"><%= label %> </th>
	    			<% end %>
    		</tr>
    		<% end %>
    	</tbody>



    </table>
    """
  end
end
