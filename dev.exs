
Logger.configure(level: :debug)


# Configures the endpoint
Application.put_env(:live_chart, DemoWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "Hu4qQN3iKzTV4fJxhorPQlA/osH9fAMtbtjVS58PFgfw3ja5Z18Q/WSNR9wP4OfW",
  live_view: [signing_salt: "hMegieSe"],
  http: [port: System.get_env("PORT") || 4000],
  debug_errors: true,
  check_origin: false,
  pubsub_server: Demo.PubSub,
  watchers: [
    node: [
      "node_modules/webpack/bin/webpack.js",
      "--mode",
      System.get_env("NODE_ENV") || "production",
      "--watch-stdin",
      cd: "assets"
    ]
  ],
  live_reload: [
    patterns: [
      ~r"priv/static/.*(js|css|png|jpeg|jpg|gif|svg)$",
      ~r"lib//live_chart.ex",
      ~r"lib/live_chart_web/(live|views)/.*(ex)$",
      ~r"lib/live_chart_web//templates/.*(ex)$"
    ]
  ]
)

defmodule DemoWeb.PageController do
    use LiveChartWeb, :controller
  import Plug.Conn

  def init(opts), do: opts

  def call(conn, :index) do
    content(conn, """
    <h2>Live Analytics Dev</h2>
    <a href="/analytics" target="_blank">Open Analytics </a>
    """)
  end

  def call(conn, :hello) do
    name = Map.get(conn.params, "name", "friend")
    content(conn, "<p>Hello, #{name}!</p>")
  end

  defp content(conn, content) do
    conn
    |> put_resp_header("content-type", "text/html")
    |> send_resp(200, "<!doctype html><html><body>#{content}</body></html>")
  end
end

defmodule DemoWeb.Router do
  use LiveChartWeb, :router
  import Phoenix.LiveView.Router

  pipeline :browser do
    plug :fetch_session
    plug :put_root_layout, {LiveChartWeb.LayoutView, :root}
   # plug :put_csp
  end

  scope "/" do
    pipe_through [:browser]

    live "/", LiveChartWeb.PageLive, :index
  end


end

defmodule LiveChartWeb.PageLive do
  use LiveChartWeb, :live_view

  @impl true
  def mount(_params, _session, socket) do

      stacks = [{"Views", "#003f5c"}, {"Visitors", "#bc5090"}]
      labels = ["Mon", "Tue", "Wed", "Thu", "Fri",  "Sat", "Sun"]
      data = [[100, 200, 200, 500, 40, 200, 0 ], [121 ,345, 212, 0, 422, 421, 0]]

      chart_input=%{stacks: stacks, labels: labels, data: data}
    {:ok, assign(socket, chart_input: chart_input, max_labels: 10)}

end

  def render(assigns) do
   ~L"""

<%= live_component @socket, LiveChart, chart_input: @chart_input, max_labels: @max_labels%>

"""
end

end


defmodule DemoWeb.Endpoint do
   use Phoenix.Endpoint, otp_app: :live_chart


  @session_options [
    store: :cookie,
    key: "_live_view_key",
    secure: false,
    signing_salt: "2uaQfmEP"
  ]

  socket "/live", Phoenix.LiveView.Socket

  socket "/phoenix/live_reload/socket", Phoenix.LiveReloader.Socket

 # Serve at "/" the static files from "priv/static" directory.
  #
  # You should set gzip to true if you are running phx.digest
  # when deploying your static files in production.
  plug Plug.Static,
    at: "/",
    from: :live_chart,
    gzip: false,
    only: ~w(css fonts images js favicon.ico robots.txt)

  plug Phoenix.LiveReloader
  plug Phoenix.CodeReloader
  plug Plug.Session, @session_options

  plug Plug.RequestId
  plug DemoWeb.Router
end

Application.ensure_all_started(:os_mon)
#Application.ensure_all_started(:live_chart)
Application.put_env(:phoenix, :serve_endpoints, true)

Task.start(fn ->
  children = [
    {Phoenix.PubSub, [name: Demo.PubSub, adapter: Phoenix.PubSub.PG2]},
    DemoWeb.Endpoint
  ]

  {:ok, _} = Supervisor.start_link(children, strategy: :one_for_one)
  Process.sleep(:infinity)
end)
